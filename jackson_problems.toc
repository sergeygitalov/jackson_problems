\contentsline {section}{\numberline {1}Introduction to Electrostatics}{1}{section.1}%
\contentsline {section}{\numberline {2}Boundary-Value Problems in Electrostatics: I}{22}{section.2}%
\contentsline {section}{\numberline {3}Boundary-Value Problems in Electrostatics: II}{87}{section.3}%
\contentsline {section}{\numberline {4}Multipoles, Electrostatics of Macroscopic Media, Dielectrics}{157}{section.4}%
\contentsline {section}{\numberline {5}Magnetostatics, Faraday's Law, Quasi-Static Fields}{162}{section.5}%
