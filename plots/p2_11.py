import numpy as np
import matplotlib.pyplot as plt

def charge_density(phi, ratio):
    return (1.0-ratio)/(ratio**2 - 2.0*ratio*np.cos(phi) + 1.0)

angle = np.arange(0.0, 2*np.pi, 0.001)

plt.plot(angle, charge_density(angle, 2.0))
plt.plot(angle, charge_density(angle, 4.0))

plt.xlabel('φ (rad)')
plt.ylabel('τ/2πb')

plt.show()

