import numpy as np
import matplotlib.pyplot as plt

def variational(x,y):
    return 1.25*x*(1-x)*y*(1-y)

#M is the series cutoff
def exact(x,y,M):
    s = 0.0
    for m in range(M+1):
        a = 1 - np.cosh((2*m+1)*np.pi*(y-0.5))/np.cosh((2*m+1)*np.pi*0.5)
        s = s + 4*(np.pi**(-3)) * np.sin((2*m+1)*np.pi*x) * ((2*m+1)**(-3)) * a

    return s

region = np.arange(0.0, 1.0, 0.001)

plt.figure()
plt.subplot(121)
plt.plot(region, variational(region, 0.25))
plt.plot(region, exact(region, 0.25,100))
plt.xlabel('x')
plt.ylabel('Φ')

plt.subplot(122)
plt.plot(region, variational(region, 0.5))
plt.plot(region, exact(region, 0.5,100))
plt.xlabel('x')

plt.show()

