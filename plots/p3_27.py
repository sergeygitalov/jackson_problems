import numpy as np
import matplotlib.pyplot as plt

def field_r(r,theta):
    return -np.cos(theta)*(1-0.5**3)**(-1)*(1-(0.5/r)**3)

def field_theta(r,theta):
    return np.sin(theta)*(1-0.5**3)**(-1)*(1+((0.5/r)**3)/2)

def field_x(r,theta):
    return field_r(r,theta)*np.cos(theta) - field_theta(r,theta)*np.sin(theta)

def field_y(r,theta):
    return field_r(r,theta)*np.sin(theta) - field_theta(r,theta)*np.cos(theta)

polar = np.radians(np.linspace(0, 360, 120))
radii = np.arange(0.5, 1.0, 0.1)

r,theta = np.meshgrid(radii,polar)
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
ax.set_theta_zero_location("N")
ax.quiver(theta, r, field_x(r,theta), field_y(r,theta))

plt.show()
