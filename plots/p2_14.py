import numpy as np
import matplotlib.pyplot as plt

def potential(r,phi):
    return 2/np.pi * np.arctan2(2*r**2*np.sin(2*phi),1-r**4)

def field_r(r,phi):
    return -(8*(r+r**5)*np.sin(2*phi))/(np.pi*(1+r**8-2*r**4*np.cos(4*phi)))

def field_phi(r,phi):
    return (8*r**2*(r**4-1)*np.cos(2*phi))/(np.pi*(1+r**8-2*r**4*np.cos(4*phi)))

def field_x(r,phi):
    return field_r(r,phi)*np.cos(phi) - field_phi(r,phi)*np.sin(phi)

def field_y(r,phi):
    return field_r(r,phi)*np.sin(phi) - field_phi(r,phi)*np.cos(phi)

azimuths = np.radians(np.linspace(0, 360, 120))
radii = np.arange(0.0, 1.0, 0.05)

r,phi = np.meshgrid(radii, azimuths)
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
ax.contour(phi, r, potential(r,phi))
ax.quiver(phi, r, field_x(r,phi), field_y(r,phi))

plt.show()

