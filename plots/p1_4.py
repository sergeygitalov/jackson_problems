import numpy as np
import matplotlib.pyplot as plt

e_0 = 8.854e-12

def e_out(r):
    return r**(-2) / (4*np.pi*e_0)

def e_2(r):
    return r / (4*np.pi*e_0)

def e_31(r):
    return r**(-1) / (4*np.pi*e_0)

def e_32(r):
    return r**3 / (4*np.pi*e_0)

inside = np.arange(0.5, 1.0, 0.001)
outside = np.arange(1.0, 2.0, 0.001)

plt.plot(outside, e_out(outside))
plt.plot(inside, e_2(inside))
plt.plot(inside, e_31(inside))
plt.plot(inside, e_32(inside))

plt.xlabel('r (m)')
plt.ylabel('E (V/m)')

plt.show()

