import numpy as np
import matplotlib.pyplot as plt
from scipy.special import eval_legendre as P

def field_inside(z):
    return z**(-2) - (3+z**(-2))/((1+z**2)**(3/2))

def field_outside(z):
    return (3+z**(-2))/((1 + z**2)**(3/2))

z_in = np.concatenate((np.arange(-1,-0.01,0.001), np.arange(0.01,1,0.001)))
z_out = np.arange(1,2,0.001)

a = plt.plot(z_in, field_inside(z_in),color='blue')
plt.plot(z_out, field_outside(z_out), color='blue')
plt.plot(-z_out, field_outside(-z_out),color='blue')
plt.xlabel('z (m)')
plt.ylabel('$E_r$ (V/m)')
plt.show()

def field_inside_r(r, theta):
    x = np.cos(theta)
    E1 = -3*x/2
    E2 = 21*r**2*P(3,x)/8
    E3 = -55*r**4*P(5,x)/16
    return E1 + E2 + E3

def field_inside_theta(r,theta):
    x = np.cos(theta)
    E1 = 3*(1-x**2)/2
    E2 = -21*r**2*(P(2,x) - x*P(3,x))/8
    E3 = 55*r**4*(P(4,x) - x*P(5,x))/16
    return E1 + E2 + E3

def field_inside_x(r,theta):
    r_proj = field_inside_r(r,theta)*np.cos(theta)
    theta_proj = -field_inside_theta(r,theta)*np.sin(theta)
    return r_proj + theta_proj 

def field_inside_y(r,theta):
    r_proj = field_inside_r(r,theta)*np.sin(theta)
    theta_proj = field_inside_theta(r,theta)*np.cos(theta)
    return r_proj + theta_proj 

def field_outside_r(r,theta):
    x = np.cos(theta)
    E1 = 3*r**(-3)*x 
    E2 = -7*r**(-5)*P(3,x)/2
    E3 = 33*r**(-7)*P(5,x)/8
    return E1 + E2 + E3

def field_outside_theta(r,theta):
    x = np.cos(theta)
    E1 = 3*(1-x**2)/2
    E2 = 21*(P(2,x) - x*P(3,x))/8
    E3 = 55*(P(4,x) - x*P(5,x))/16
    return E1 + E2 + E3

def field_outside_x(r,theta):
    r_proj = field_outside_r(r,theta)*np.cos(theta)
    theta_proj = -field_outside_theta(r,theta)*np.sin(theta)
    return r_proj + theta_proj 

def field_outside_y(r,theta):
    r_proj = field_outside_r(r,theta)*np.sin(theta)
    theta_proj = field_outside_theta(r,theta)*np.cos(theta)
    return r_proj + theta_proj 

angle = np.radians(np.linspace(0,360,60))
radii_in = np.arange(0,1.0,0.1)
radii_out = np.arange(1,2.0,0.1)

r_in,theta = np.meshgrid(radii_in, angle)
r_out,theta = np.meshgrid(radii_out, angle)
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
ax.set_theta_zero_location("N")
ax.quiver(theta,r_in,field_inside_x(r_in,theta),field_inside_y(r_in,theta))
ax.quiver(theta,r_out,field_outside_x(r_out,theta),field_outside_y(r_out,theta))

plt.show()

