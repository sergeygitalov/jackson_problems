import numpy as np
import matplotlib.pyplot as plt

e_0 = 8.854e-12

def plate(rho):
    return e_0*(1 - rho**(-3))

def boss(theta):
    return 3*e_0*np.cos(theta)

radial = np.arange(1, 5, 0.001)
angle = np.arange(0, np.pi/2, 0.001)

plt.figure()
plt.subplot(211)
plt.plot(radial, plate(radial))
plt.xlabel('ρ (m)')
plt.ylabel('σ (C/$m^2$)')

plt.subplot(212)
plt.plot(angle, boss(angle))
plt.xlabel('θ (rad)')
plt.ylabel('σ (C/$m^2$)')

plt.tight_layout()
plt.show()
