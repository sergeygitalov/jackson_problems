import numpy as np
import matplotlib.pyplot as plt

def charge_density(r):
    return - (2*np.pi)**(-1) * (r**2 + 1)**(-3/2)

domain = np.arange(0.0, 5.0, 0.001)

plt.plot(domain, charge_density(domain))

plt.xlabel('ρ (m)')
plt.ylabel('σ ($C/m^2$)')

plt.show()

