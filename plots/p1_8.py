import numpy as np
import matplotlib.pyplot as plt

e_0 = 8.854e-12

def plates(z):
    return z*0 + e_0/2

def spheres(r):
    return r**(-4) * e_0/2 / (4*np.pi)**2

def cylinders(rho):
    return rho**(-2) * e_0/2 / (2*np.pi)**2

t = np.arange(0.5, 2, 0.001)

plt.figure()
plt.subplot(131)
plt.plot(t, plates(t))
plt.xlabel('z (m)')
plt.ylabel('w ($J/m^3$)')

plt.subplot(132)
plt.plot(t, spheres(t))
plt.xlabel('r (m)')

plt.subplot(133)
plt.plot(t, cylinders(t))
plt.xlabel('ρ (m)')

plt.show()

