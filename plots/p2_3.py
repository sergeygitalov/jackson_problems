import numpy as np
import matplotlib.pyplot as plt

def charge_density1(x):
    return np.pi**(-1) * (((x+1)**2 + 1)**(-1) - ((x-1)**2 + 1)**(-1))

def charge_density2(x):
    return 2*np.pi**(-1) * (((x+1)**2 + 4)**(-1) - ((x-1)**2 + 4)**(-1))

domain = np.arange(0.0, 10.0, 0.001)

plt.plot(domain, charge_density1(domain))
plt.plot(domain, charge_density2(domain))

plt.xlabel('x')
plt.ylabel('σ/λ')

plt.show()

