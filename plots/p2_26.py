import numpy as np
import matplotlib.pyplot as plt

e_0 = 8.854e-12

def half_cylinder(phi):
    return e_0 * 2*np.sin(phi)

def plane(rho):
    return e_0 * (1 - rho**(-2))

rho = np.arange(1, 10, 0.001)
phi = np.arange(0, 2*np.pi, 0.001)

plt.figure()
plt.subplot(211)
plt.plot(rho, plane(rho))
plt.xlabel('ρ (m)')
plt.ylabel('σ (C/$m^2$)')

plt.subplot(212)
plt.plot(phi, half_cylinder(phi))
plt.xlabel('φ (rad)')
plt.ylabel('σ (C/$m^2$)')

plt.tight_layout()
plt.show()

