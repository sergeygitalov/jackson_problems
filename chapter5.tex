\documentclass[jackson_problems.tex]{subfiles}

\usepackage{enumerate}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{bbm}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{multicol}
\usepackage{slashed}
\usepackage{hyperref}
\usepackage{siunitx}
\usepackage{esint}

\numberwithin{equation}{section}

\begin{document}

\begin{problem}\label{problem:5_1}
Starting with the differential expression
\begin{equation*}
    d\mathbf{B} = \frac{1}{4\pi} Id\mathbf{l}' \times
    \frac{\mathbf{x}-\mathbf{x}'}{|\mathbf{x}-\mathbf{x}'|^3}
\end{equation*}
for the magnetic induction at the point $P$ with coordinate $\mathbf{x}$
produced by an increment of current $Id\mathbf{l}'$ at $\mathbf{x}'$, show
explicitly that for a closed loop carrying a current $I$ the magnetic induction
at $P$ is
\begin{equation*}
    \mathbf{B} = \frac{1}{4\pi}I\nabla\Omega
\end{equation*}
where $\Omega$ is the solid angle subtended by the loop at the point $P$. This
corresponds to a magnetic scalar potential, $\Phi_M \equiv -I\Omega/4\pi$. The
sign convention for the solid angle is that $\Omega$ is positive if the point
$P$ views the ``inner'' side of the surface spanning the loop, that is, if a
unit normal $\hat{n}$ to the surface is defined by the direction of current flow
via the right-hand rule, $\Omega$ is positive if $\hat{n}$ points away from the
point $P$, and negative otherwise. This is the same convention as in Section 1.6
for the electric dipole layer.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_2}
A long, right cylindrical, ideal solenoid of arbitrary cross section is created 
by stacking a large number of identical current-carrying loops one above the 
other, with $N$ coils per unit length and each loop carrying a current $I$.
\begin{enumerate}[label=(\alph*)]
    \item In the approximation that the solenoidal coil is an ideal current
    sheet and infinitely long, use Problem \ref{problem:5_1} to establish that
    at any point inside the coil the magnetic field is axial and equal to
    \begin{equation*}
        H = NI
    \end{equation*}
    and that $H = 0$ for any point outside the coil.
    \item For a realistic solenoid of circular cross section of radius $a$
    ($Na \gg 1$), but still infinite in length, show that the ``smoothed''
    magnetic field just outside the solenoid (averaged axially over several
    turns) is not zero, but is the same in magnitude and direction as that of a
    single wire on the axis carrying a current $I$, even if 
    $Na\rightarrow\infty$. Compare fields inside and out.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_3}
    A right-circular solenoid of finite length $L$ and radius $a$ has $N$ turns
    per unit length and carries a current $I$. Show that the magnetic induction
    on the cylinder axis in the limit $NL\rightarrow\infty$ is
    \begin{equation*}
        B_z = \frac{NI}{2}\left(\cos\theta_1 + \cos\theta_2\right)
    \end{equation*}
    where $\theta_1$ and $\theta_2$ are angles between the cylinder
    axis and the lines from the point at $z$ to the edges of the solenoid.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_4}
A magnetic induction $\mathbf{B}$ in a current-free region in a uniform medium 
is cylindrically symmetric with components $B_z(\rho,z)$ and $B_\rho(\rho,z)$ 
and with a known $B_z(0,z)$ on the axis of symmetry. The magnitude of the axial 
field varies slowly in $z$.
\begin{enumerate}[label=(\alph*)]
    \item Show that near the axis the axial and radial components of magnetic
    induction are approximately
    \begin{align*}
        B_z(\rho,z) &\approx B_z(0,z) 
        - \frac{\rho^2}{4}\frac{\partial^2B_z(0,z)}{\partial{z^2}} + \cdots \\
        B_\rho(\rho,z) &\approx 
        - \frac{\rho}{2}\frac{\partial B_z(0,z)}{\partial{z}}
        + \frac{\rho^3}{16}\frac{\partial^3 B_z(0,z)}{\partial z^3} + \cdots
    \end{align*}
    \item What are the magnitudes of the neglected terms, or equivalently what
    is the criterion defining ``near'' the axis?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
\hspace{2em}
\begin{enumerate}[label=(\alph*)]
    \item Use the results of Problems \ref{problem:5_4} and \ref{problem:5_3} to
    find the axial and radial components of magnetic induction in the central
    region ($|z| \ll L/2$) of a long uniform solenoid of radius $a$ and ends at
    $z = \pm L/2$, including the value of $B_z$ just inside the coil 
    ($\rho=a^-$).
    \item Use Ampere's law to show that the longitudinal magnetic induction just
    outside the coil is approximately
    \begin{equation*}
        B_z(\rho=a^+, z) \approx -\frac{2NIa^2}{L^2}
        \left(1 + \frac{12z^2}{L^2} - \frac{9a^2}{L^2} + \cdots\right)
    \end{equation*}
    For $L \ll a$, the field outside is negligible compared to the inside. How
    does this axial component compare in size to the azimuthal component of
    Problem \ref{problem:5_2}b?
    \item Show that at the end of the solenoid the magnetic induction near the
    axis has components
    \begin{equation*}
        B_z \approx \frac{NI}{2}, \ B_\rho \approx \pm\frac{NI}{4}\frac{\rho}{a}
    \end{equation*}
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A cylindrical conductor of radius $a$ has a hole of radius $b$ bored parallel 
to, and centered a distance $d$ from, the cylinder axis ($d+b<a$). The current
density is uniform throughout the remaining metal of the cylinder and is
parallel to the axis. Use Ampere's law and principle of superposition to find
the magnitude and the direction of the magnetic-flux density in the hole.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_7}
A compact circular coil of radius $a$, carrying a current $I$ (perhaps $N$
turns, each with current $I/N$), lies in the $x-y$ plane with its center at the
origin.
\begin{enumerate}[label=(\alph*)]
    \item Find the magnetic induction at any point on the $z$ axis using the
    Biot-Savart law.
    \item An identical coil with the same magnitude and sense of the current is
    located on the same axis, parallel to, and a distance $b$ above, the first
    coil. With the coordinate origin relocated at the point midway between the
    centers of the two coils, determine the magnetic induction on the axis near
    the origin as an expansion in powers of $z$, up to $z^4$ inclusive:
    \begin{equation*}
        B_z = \frac{Ia^2}{d^3}
        \left[1 + \frac{3(b^2-a^2)z^2}{2d^4} 
        + \frac{15(b^4-6b^2a^2+2a^4)z^4}{16d^8} + \cdots\right]
    \end{equation*}
    where $d^2 = a^2 + b^2/4$.
    \item Show that, off-axis near the origin, the axial and radial components,
    correct to second order in the coordinates, take the form
    \begin{equation*}
        B_z = \sigma_0 + \sigma_2\left(z^2 - \frac{\rho^2}{2}\right); \
        B_\rho = -\sigma_2z\rho
    \end{equation*}
    \item For the two coils in part b show that the magnetic induction on the
    $z$ axis for large $|z|$ is given by the expansion in inverse odd powers of
    $|z|$ obtained from the small $z$ expansion of part d by the formal
    substitution, $d\rightarrow|z|$.
    \item If $b=a$, the two coils are known as a pair of Helmholtz coils. For
    this choice of geometry the second terms in the expansions of parts a and
    d are absent ($\sigma_2=0$ in part c). The field near the origin is then
    very uniform. What is the maximum permitted value of $|z|/a$ if the axial
    field is to be uniform to one part in $10^4$, on part in $10^2$?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_8}
A localized cylindrically symmetric current distribution is such that the
current flows only in the azimuthal direction; the current density is a function
only of $r$ and $\theta$ (or $\rho$ and $z$): 
$\mathbf{J} = \hat{\phi}J(\rho,\theta)$. The distribution is ``hollow'' in the
sense that there is a current-free region near the origin, as well as outside.
\begin{enumerate}[label=(\alph*)]
    \item Show that the magnetic field can be derived from the azimuthal
    component of the vector potential, with a multipole expansion
    \begin{equation*}
        A_\phi(\rho,\theta) = -\frac{1}{4\pi}\sum_l m_l r^l P_l^1(\cos\theta)
    \end{equation*}
    in the interior and
    \begin{equation*}
        A_\phi(\rho,\theta) = -\frac{1}{4\pi}\sum_l \mu_l r^{-l-1}P_l^1(\cos\theta)
    \end{equation*}
    outside the current distribution.
    \item Show that the internal and external multipole moments are
    \begin{equation*}
        m_l = -\frac{1}{l(l+1)}\int d^3x r^{-l-1}P_l^1(\cos\theta)J(\rho,\theta)
    \end{equation*}
    and
    \begin{equation*}
        \mu_l = -\frac{1}{l(l+1)}\int d^3x r^l P_l^1(\cos\theta)J(\rho,\theta)
    \end{equation*}
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
The two circular coils of radius $a$ and separation $b$ of Problem 
\ref{problem:5_7} can be described in cylindrical coordinates by the current
density
\begin{equation*}
    \mathbf{J} = \hat{\phi}I\delta(\rho-a)[\delta(z-b/2) + \delta(z+b/2)]
\end{equation*}
\begin{enumerate}[label=(\alph*)]
    \item Using the formalism of Problem \ref{problem:5_8}, calculate the
    internal and external multiple moments for $l=1,\ldots,5$.
    \item Using the internal multipole expansion of Problem \ref{problem:5_8},
    write down explicitly and expression for $B_z$ on the $z$ axis and relate
    it to the answer of Problem \ref{problem:5_7}b.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_10}
A circular current loop of radius $a$ carrying a current $I$ lies in the $x-y$
plane with its center at the origin.
\begin{enumerate}[label=(\alph*)]
    \item Show that the only nonvanishing component of the vector potential is
    \begin{equation*}
        A_\phi(\rho,z) = \frac{Ia}{\pi}
        \int_0^\infty dk\cos(kz)I_1(k\rho_<)K_1(k\rho_>)
    \end{equation*}
    where $\rho_<$ ($\rho_>$) is the smaller (larger) of $a$ and $\rho$.
    \item Show that an alternative expression for $A_\phi$ is
    \begin{equation*}
        A_\phi(\rho,z) = \frac{Ia}{2}
        \int_0^\infty dk e^{-k|z|}J_1(ka)J_1(k\rho)
    \end{equation*}
    \item Write down expressions for the components of magnetic induction, using
    the expressions of parts a and b. Evaluate explicitly the components of
    $\mathbf{B}$ on the $z$ axis by performing the necessary integrations.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_11}
A circular loop of wire carrying a current $I$ is located with its center at the
origin of coordinates and the normal to its plane having spherical angles
$(\theta_0,\phi_0)$. There is an applied magnetic field, 
$B_x = B_0(1+\beta\gamma)$ and $B_y = B_0(1_\beta x)$.
\begin{enumerate}[label=(\alph*)]
    \item Calculate the force acting on the loop without making any 
    approximations. Compare your result with the approximate result,
    \begin{equation*}
        \mathbf{F} = \nabla(\mathbf{m}\cdot\mathbf{B})
    \end{equation*}
    Comment.
    \item Calculate the torque in lowest order. Can you deduce anything about
    the higher order contributions? Do they vanish for the circular loop? What
    about for other shapes?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Two concentric circular loops of radii $a,b$ and currents $I,I'$, respectively
($b<a$), have an angle $\alpha$ between their planes. Show that the torque on
one of the loops is about the line of intersection of the two planes containing
the loops and has the magnitude
\begin{equation*}
    N = \frac{\pi II'b^2}{2a}\sum_{n=0}^\infty\frac{n+1}{2n+1}
    \left[\frac{\Gamma(n+3/2)}{\Gamma(n+2)\Gamma(3/2)}\right]^2
    \left(\frac{b}{a}\right)^{2n} P_{2n+1}^1(\cos\alpha)
\end{equation*}
Determine the sense of the torque for $\alpha$ an acute angle and the currents
in the same (opposite) direction.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A sphere of radius $a$ carries a uniform surface-charge distribution $\sigma$. 
The sphere is rotated about a diameter with constant angular velocity $\omega$. 
Find the vector potential and magnetic-flux density both inside and outside the 
sphere.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_14}
A long, hollow, right circular cylinder of inner (outer) radius $a$ ($b$), and
of relative permeability $\mu_r$, is placed in a region of initially uniform
magnetic-flux density $\mathbf{B}_0$ at right angles to the field. Find the flux
density at all points in space, and sketch the logarithm of the ratio of the
magnitudes of $\mathbf{B}$ on the cylinder axis to $\mathbf{B}_0$ as a function
of $\log_{10}\mu_r$ for $a^2/b^2 = 0.5, \ 0.1$. Neglect the end effects.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Consider two long, straight wires, parallel to the $z$ axis, spaced a distance
$d$ apart and carrying currents $I$ in opposite directions. Describe the
magnetic field $\mathbf{H}$ in terms of a magnetic scalar potential $\Phi_M$,
with $\mathbf{H} = -\nabla\Phi_M$.
\begin{enumerate}[label=(\alph*)]
    \item If the wires are parallel to the $z$ axis with positions $x=\pm d/2$,
    $y=0$, show that in the limit of small spacing, the potential is
    approximately that of a two-dimensional dipole,
    \begin{equation*}
        \Phi_M \approx -\frac{Id}{2\pi\rho}\sin\phi + \mathcal{O}(d^2/\rho^2)
    \end{equation*}
    \item The closely spaced wires are now centered in a hollow right-circular
    cylinder of steel, of inner (outer) radius $a$ ($b$) and magnetic
    permeability $\mu = \mu_r\mu_0$. Determine the magnetic scalar potential in
    the three regions, $0<\rho<a$, $a<\rho<b$, and $\rho<b$. Show that the field
    outside the steel cylinder is a two-dimensional dipole field, as in part a,
    but with a strength reduced by the factor
    \begin{equation*}
        F = \frac{4\mu_rb^2}{(\mu_r+1)^2b^2 - (\mu_r-1)^2a^2}
    \end{equation*}
    Relate your result to Problem \ref{problem:5_14}.
    \item Assuming that $\mu_r \gg 1$, and $b=a+t$, where the thickness 
    $t \ll b$, write down an approximate expression for $F$ and determine its
    numerical value for $\mu_r=200$ (typical of steel at 20 gauss), 
    $b = \qty{1.25}{\cm}$, $t=\qty{3}{\mm}$. The shielding effect is relevant
    for reduction of stray fields in residential and commercial $\qty{60}{\Hz}$,
    110 or $\qty{220}{\V}$ wiring.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A circular loop of wire of radius $a$ and negligible thickness carries a current
$I$. The loop is centered in a spherical cavity of radius $b>a$ in a large block
of soft iron. Assume that the relative permeability of the iron is effectively
infinite and that of the medium in the cavity, unity.
\begin{enumerate}[label=(\alph*)]
    \item In the approximation of $b \gg a$, show that the magnetic field at the
    center of the loop is augmented by a factor $(1+a^3/2b^3)$ by the presence
    of the iron.
    \item What is the radius of the ``image'' current loop (carrying the same
    current) that simulates the effect of the iron for $r<b$?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A current distribution $\mathbf{J}(\mathbf{x})$ exists in a medium of unit
relative permeability adjacent to a semi-infinite slab of material having
relative permeability $\mu_r$ and filling the half-space, $z<0$.
\begin{enumerate}[label=(\alph*)]
    \item Show that for $z>0$ the magnetic induction can be calculated by
    replacing the medium of permeability $\mu_r$ by an image current
    distribution, $\mathbf{J}^\ast$, with components
    \begin{equation*}
        \frac{\mu_r-1}{\mu_r+1} J_x(x,y,-z), \
        \frac{\mu_r-1}{\mu_r+1} J_y(x,y,-z), \
        -\frac{\mu_r-1}{\mu_r+1} J_z(x,y,-z)
    \end{equation*}
    \item Show that for $z<0$ the magnetic induction appears to be due to a
    current distribution $[2\mu_r/(\mu_r+1)]\mathbf{J}$ in a medium of unit
    relative permeability.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_18}
A circular loop of wire having a radius $a$ and carrying a current $I$ is
located in vacuum with its center a distance $d$ away from a semi-infinite slab
of permeability $\mu$. Find the force acting on the loop when
\begin{enumerate}[label=(\alph*)]
    \item the plane of the loop is parallel to the face of the slab,
    \item the plane of the loop is perpendicular to the face of the slab.
    \item Determine the limiting from of your answer to parts a and b when
    $d \ll a$. Can you obtain these limiting values in some simple and direct
    way?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A magnetically ``hard'' material is in the shape of a right circular cylinder
of length $L$ and radius $a$. The cylinder has a permanent magnetization $M_0$,
uniform throughout its volume and parallel to its axis.
\begin{enumerate}[label=(\alph*)]
    \item Determine the magnetic field $\mathbf{H}$ and magnetic induction
    $\mathbf{B}$ at all points on the axis of the cylinder, both inside and
    outside.
    \item Plot the rations $\mathbf{B}/M_0$ and $\mathbf{H}/M_0$ on the axis
    as functions of $z$ for $L/a = 5$.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
\hspace{2em}
\begin{enumerate}[label=(\alph*)]
    \item Starting from the force equation
    \begin{equation*}
        \mathbf{F} = \int \mathbf{J}(\mathbf{x})\times\mathbf{B}(\mathbf{x})d^3x
    \end{equation*}
    and the fact that a magnetization $\mathbf{M}$ inside a volume $V$ bounded
    by a surface $S$ is equivalent to a volume current density
    $\mathbf{J}_M = \nabla\times\mathbf{M}$ and a surface current density
    $\nabla\times\hat{n}$, show that in the absence of macroscopic conduction
    currents the total magnetic force on the body can be written
    \begin{equation*}
        \mathbf{F} = -\int_V (\nabla\cdot\mathbf{M})\mathbf{B}_e d^3x
        + \int_S (\mathbf{M}\cdot\hat{n})\mathbf{B}_e da
    \end{equation*}
    where $\mathbf{B}_e$ is the applied magnetic induction. The force is now
    expressed in terms of the effective charge densities $\rho_M$ and 
    $\sigma_M$. If the distribution of magnetization is not discontinuous, the
    surface can be at infinity and the force given by just the volume integral.
    \item A sphere of radius $R$ with uniform magnetization has its center at
    the origin of coordinates and its direction of magnetization making
    spherical angles $\theta_0,\phi_0$. If the external magnetic field is the
    same as in Problem \ref{problem:5_11}, use the expression of part a to
    evaluate the components of the force acting on the sphere.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A magnetostatic field is due entirely to a localized distribution of permanent
magnetization.
\begin{enumerate}[label=(\alph*)]
    \item Show that
    \begin{equation*}
        \int_{\mathbb{R}^3} \mathbf{B}\cdot\mathbf{H} d^3x = 0
    \end{equation*}
    \item From the potential energy $U = -\mathbf{m}\cdot\mathbf{B}$ of a dipole
    in an external field, show that for a continuous distribution of permanent
    magnetization the magnetostatic energy can be written
    \begin{equation*}
        W = \frac{1}{2}\int |\mathbf{H}|^2 d^3x 
        = -\frac{1}{2} \int \mathbf{M}\cdot\mathbf{H} d^3x
    \end{equation*}
    apart from an additive constant, which is independent of the orientation or
    position of the various constituent magnetized bodies.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Show that in general a long, straight bar of uniform cross-sectional area $A$
with uniform lengthwise magnetization $M$, when placed with its flat end against
an infinitely permeable flat surface, adheres with a force given approximately
by
\begin{equation*}
    F \approx \frac{1}{2}AM^2
\end{equation*}
Relate your discussion to the electrostatic considerations in Section 1.11.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A right circular cylinder of length $L$ and radius $a$ has a uniform lengthwise
magnetization $M$.
\begin{enumerate}[label=(\alph*)]
    \item Show that, when it is placed with its flat end against an infinitely
    permeable plane surface, it adheres with a force
    \begin{equation*}
        F = 2aLM^2
        \left[\frac{K(k) - E(k)}{k} - \frac{K(k_1) - E(k_1)}{k_1}\right]
    \end{equation*}
    where
    \begin{equation*}
        k = \frac{2a}{\sqrt{4a^2 + L^2}}, \
        k_1 = \frac{a}{\sqrt{a^2 + L^2}}
    \end{equation*}
    \item Find the limiting form of the force if $L \gg a$.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
\hspace{2em}
\begin{enumerate}[label=(\alph*)]
    \item For the perfectly conducting plane of Section 5.13 with the circular
    hole in it and the asymptotically uniform tangential magnetic field
    $\mathbf{H}_0$ on one side, calculate the added tangential magnetic field
    $\mathbf{H}^{(1)}$ on the side of the plane with $\mathbf{H}_0$. Show that
    the components for $\rho > a$ are
    \begin{equation*}
    \begin{split}
        H^{(1)}_x &= \frac{2H_0}{\pi}\frac{a^3 xy}{\rho^4\sqrt{\rho^2-a^2}} \\
        H^{(1)}_y &= \frac{2H_0}{\pi}\frac{a^3 y^2}{\rho^4\sqrt{\rho^2-a^2}}
        + \frac{H_0}{\pi}
        \left(\frac{a}{\rho}\sqrt{1-\frac{a^2}{\rho^2}}
        - \sin^{-1}\frac{a}{\rho}\right)
    \end{split}
    \end{equation*}
    \item Sketch the lines of surface current flow in the neighborhood of the
    hole on both sides of the plane.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A flat right rectangular loop carrying a constant current $I_1$ is placed near
a long straight wire carrying a current $I_2$. The loop is oriented so that its
center is a perpendicular distance $d$ from the wire; the sides of length $a$
are parallel to the wire and the sides of length $b$ make an angle $\alpha$ with
the plane containing the wire and the loop's center. The direction of the
current $I_1$ is the same as that of $I_2$ in the side of the rectangle nearest
the wire.
\begin{enumerate}[label=(\alph*)]
    \item Show that the interaction magnetic energy
    \begin{equation*}
        W_{12} = \int \mathbf{J}\cdot\mathbf{A}_2 d^3x = I_1F_2
    \end{equation*}
    (where $F_2$ is the magnetic flux from $I_2$ linking the rectangular circuit
    carrying $I_1$), is
    \begin{equation*}
        W_{12} = \frac{I_1I_2a}{4\pi}
        \ln\frac{4d^2+b^2+4db\cos\alpha}{4d^2+b^2-4db\cos\alpha}
    \end{equation*}
    \item Calculate the force between the loop and the wire of fixed currents.
    \item Repeat the calculation for a circular loop of radius $a$, whose plane
    is parallel to the wire and makes an angle $\alpha$ with respect to the
    plane containing the center of the loop and the wire. Show that the
    interaction energy is
    \begin{equation*}
        W_{12} = I_1I_2d\mathrm{Re}(e^{i\alpha} - \sqrt{e^{2i\alpha}-a^2/d^2})
    \end{equation*}
    Find the force.
    \item For both loops, show that when $d\gg a,b$ the interaction energy
    reduced to $W_{12} \approx \mathbf{m}\cdot\mathbf{B}$, where $\mathbf{m}$ is
    the magnetic moment of the loop. Explain the sign.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A two-wire transmission line consists of a pair of nonpermeable parallel wires
of radii $a$ and $b$ separated by a distance $d>a+b$. A current flows down one
wire and back the other. It is uniformly distributed over the cross section of
each wire. Show that the self-inductance per unit length is
\begin{equation*}
    L = \frac{1}{4\pi}\left(1 + 2\ln\frac{d^2}{ab}\right)
\end{equation*}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A circuit consists of a long thin conducting shell of radius $a$ and a parallel
return wire of radius $b$ on axis inside. If the current is assumed distributed
uniformly throughout the cross section of the wire, calculate the 
self-inductance per unit length. What is the self-inductance if the inner
conductor is a thin hollow tube?
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Show that the mutual inductance of two circular coaxial loops in a homogeneous
medium of permeability $\mu$ is
\begin{equation*}
    M_{12} = \mu\sqrt{ab}\left[\left(\frac{2}{k}-k\right)K(k)
    - \frac{2}{k}E(k)\right]
\end{equation*}
where
\begin{equation*}
    k^2 = \frac{4ab}{(a+b)^2+d^2}
\end{equation*}
and $a$, $b$ are the radii of the loops, $d$ is the distance between their
centers, and $K$ and $E$ are the complete elliptic integrals.

Find the limiting value when $d\ll a, \ b$ and $a\approx b$.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Consider a transmission line consisting of two, parallel perfect conductors of
arbitrary, but constant cross section. Current flows down one conductor and
returns via the other. Show that the product of inductance per unit length $L$
and the capacitance per unit length $C$ is $LC = \mu\epsilon$, where $\mu$ and
$\epsilon$ are the permeability and the permittivity of the medium surrounding
the conductors. (See the discussion about magnetic fields near perfect
conductors at the beginning of Section 5.13.)
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_30}
\hspace{2em}
\begin{enumerate}[label=(\alph*)]
    \item Show that a surface current density $K(\phi) = I\cos(\phi)/2R$ flowing
    in the axial direction on a right circular cylindrical surface of radius $R$
    produces inside the cylinder a uniform magnetic induction $B_0 = I/4R$ in a
    direction perpendicular to the cylinder axis. Show that the field outside is
    that of a two-dimensional dipole.
    \item Calculate the total magnetostatic field energy per unit length. How is
    it divided inside and outside the cylinder?
    \item What is the inductance per unit length of the system, viewed as a long
    circuit with current flowing up one side of the cylinder and back the other?
    [Answer: $L=\pi/8$.]
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
An accelerator bending magnet consists of $N$ turns of superconducting cable
whose current configuration can be described approximately by the axial current
density
\begin{equation*}
    J_z(\rho,\phi) = \frac{NI}{2R}\cos\phi \delta(\rho - R)
\end{equation*}
The right circular current cylinder is centered on the axis of a hollow iron
cylinder of inner radius $R'$ ($R'>R$). The relative dimensions ($R, \ R'$ a few
centimeters and a magnet length of several meters) permit the use of a
two-dimensional approximation, at least away from the ends of the magnet. Assume
that the relative permeability of the iron can be taken as infinite.
[Then the outer radius of the iron is irrelevant.]
\begin{enumerate}[label=(\alph*)]
    \item Show that the magnetic field inside the current sheath is 
    perpendicular to the axis of the cylinder in the direction defined by
    $\phi = \pm\pi/2$ and has the magnitude
    \begin{equation*}
        B_0 = \frac{NI}{4R}\left(1 + \frac{R^2}{R^{\prime 2}}\right)
    \end{equation*}
    \item Show that the magnetic energy inside $r=R$ is augmented (and that
    outside is diminished) relative to the values in the absence of the iron.
    (Compare part b of Problem \ref{problem:5_30}.)
    \item Show that the inductance per unit length is
    \begin{equation*}
        \frac{dL}{dz} = \frac{\pi N^2}{8}\left(1 + \frac{R^2}{R^{\prime 2}}\right)
    \end{equation*}
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
A circular loop of mean radius $a$ is made of wire having a circular cross
section of radius $b$, with $b\ll a$.
\begin{enumerate}[label=(\alph*)]
    \item Using the expression for the vector potential of a filamentary
    circular loop,
    \begin{equation*}
        A_\phi(\rho,\theta) = \frac{Ia}{\pi\sqrt{a^2+r^2+2ar\sin\theta}}
        \frac{(2-k^2)K(k) - 2E(k)}{k^2}
    \end{equation*}
    with
    \begin{equation*}
        k^2 = \frac{4ar\sin\theta}{a^2+r^2+2ar\sin\theta}
    \end{equation*}
    and appropriate approximations for the elliptic integrals, show that the
    vector potential at the point $P$ near the wire is approximately
    \begin{equation*}
        A_\phi = \frac{I}{2\pi}\left(\ln\frac{8a}{\rho} - 2\right)
    \end{equation*}
    where $\rho$ is the transverse coordinate and corrections are of order
    $(\rho/a)\cos\phi$ and $(\rho/a)^2$.
    \item Since the vector potential of part a is, apart from a constant, just
    that outside a straight circular wire carrying a current $I$, determine the
    vector potential inside the wire ($\rho < b$) in the same approximation by
    requiring continuity of $A_\phi$ and its radial derivative at $\rho = b$,
    assuming that the current is uniform in density inside the wire:
    \begin{equation*}
        A_\phi = \frac{I}{4\pi}\left(1 - \frac{\rho^2}{b^2}\right)
        + \frac{I}{2\pi}\left(\ln\frac{8a}{b} - 2\right), \ \rho < b
    \end{equation*}
    \item Use
    \begin{equation*}
        W = \frac{1}{2}\int \mathbf{J}\cdot\mathbf{A} d^3x
    \end{equation*}
    to find the magnetic energy, hence the self-inductance,
    \begin{equation*}
        L = a\left(\ln\frac{8a}{b} - \frac{7}{4}\right)
    \end{equation*}
    Are the corrections of order $b/a$ or $(b/a)^2$? What is the change in $L$
    if the current is assumed to flow only on the surface of the wire (as occurs
    at high frequencies when the skin depth is small compared to $b$)?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Consider two current loops whose orientation in space is fixed, but whose 
relative separation can be changed. Let $O_1$ and $O_2$ be origins in the two
loops, fixed relative to each loop, and $\mathbf{x}_1$ and $\mathbf{x}_2$ be
coordinates of elements $d\mathbf{I}_1$ and $d\mathbf{I}_2$ respectively, of the
loops referred to the respective origins. Let $\mathbf{R}$ be the relative
coordinate of the origins, directed from loop 2 to loop 1.
\begin{enumerate}[label=(\alph*)]
    \item Starting from the expression for the force between the loops,
    \begin{equation*}
        \mathbf{F}_{12} = -\frac{I_1I_2}{4\pi}
        \oiint \frac{(d\mathbf{I}_1\cdot d\mathbf{I}_2)\mathbf{x}_{12}}{|\mathbf{x}_{12}|^3}
    \end{equation*}
    show that it can be written
    \begin{equation*}
        \mathbf{F}_{12} = I_1I_2\nabla_R M_{12}(\mathbf{R})
    \end{equation*}
    where $M_{12}$ is the mutual inductance of the loops,
    \begin{equation*}
        M_{12}(\mathbf{R}) = \frac{1}{4\pi}
        \oiint \frac{d\mathbf{I}_1\cdot d\mathbf{I}_2}{|\mathbf{x}_1-\mathbf{x}_2+\mathbf{R}|}
    \end{equation*}
    and it is assumed that the orientation of the loops does not change with
    $\mathbf{R}$.
    \item Show that the mutual inductance, viewed as a function of $\mathbf{R}$,
    is a solution of the Laplace equation,
    \begin{equation*}
        \nabla_R^2 M_{12}(\mathbf{R}) = 0
    \end{equation*}
    The importance of this result is that the uniqueness of solutions of the
    Laplace equation allows the exploitation of the properties of such 
    solutions, provided a solution can be found for a particular value of
    $\mathbf{R}$.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
Two identical circular loops of radius $a$ are initially located a distance $R$ 
apart on a common axis perpendicular to their planes.
\begin{enumerate}[label=(\alph*)]
    \item From the expression $W_{12} = \int d^3x \mathbf{J}_1\cdot\mathbf{A}_2$
    and the result for $A_\phi$ from Problem \ref{problem:5_10}b, show that
    the mutual inductance of the loops is
    \begin{equation*}
        M_{12} = \pi a^2 \int_0^\infty dk e^{-kR}J_1^2(ka)
    \end{equation*}
    \item Show that for $R > 2a$, $M_{12}$ has the expansion
    \begin{equation*}
        M_{12} = \frac{\pi a^4}{2R^3}
        \left[1 - 3\left(\frac{a}{R}\right)^2
        + \frac{75}{8}\left(\frac{a}{R}\right)^4 + \cdots\right]
    \end{equation*}
    \item Use the techniques of Section 3.3 for solutions of the Laplace
    equation to show that the mutual inductance of two coplanar identical
    circular loops of radius $a$ whose centers are separated by a distance
    $R > 2a$ is
    \begin{equation*}
        M_{12} = -\frac{\pi a^4}{4R^3}
        \left[1 + \frac{9}{4}\left(\frac{a}{R}\right)^2
        + \frac{375}{64}\left(\frac{a}{R}\right)^4 + \cdots\right]
    \end{equation*}
    \item Calculate the forces between the loops in the common axis and coplanar
    configurations. Relate the answers to those of Problem \ref{problem:5_18}.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}\label{problem:5_35}
An insulated coil is wound on the surface of a sphere of radius $a$ in such a
way as to produce a uniform magnetic induction $B_0$ in the $z$ direction inside
the sphere and dipole field outside the sphere. The medium inside and outside
the sphere has a uniform conductivity $\sigma$ and permeability $\mu$.
\begin{enumerate}[label=(\alph*)]
    \item Find the necessary surface current density $\mathbf{K}$ and show that
    the vector potential describing the magnetic field has only an azimuthal
    component, given by
    \begin{equation*}
        A_\phi = \frac{B_0a^2}{2}\frac{r_<}{r_>^2} \sin\theta
    \end{equation*}
    where $r_<$ ($r_>$) is the smaller (larger) of $r$ and $a$.
    \item At $t=0$ the current in the coil is cut off. [The coil's presence may
    be ignored from now on.] With the neglect of Maxwell's displacement current,
    the decay of the magnetic field is described by the diffusion equation
    \begin{equation*}
        \nabla^2\mathbf{A} = \mu\sigma\frac{\partial\mathbf{A}}{\partial{t}}
    \end{equation*}
    Using a Laplace transform and a spherical Bessel function expansion
    \begin{equation*}
        A(r) = \int_0^\infty A(k)j_l(kr)dk
    \end{equation*}
    with
    \begin{equation*}
        A(k) = \frac{2k^2}{\pi}\int_0^\infty r^2 A(r) j_l(kr) dr
    \end{equation*}
    show that the vector potential at times $t>0$ is given by
    \begin{equation*}
        A_\phi = \frac{3B_0a}{\pi}\sin\theta\int_0^\infty e^{-\nu tk^2}
        j_1(k)j_1(kr/a)dk
    \end{equation*}
    where $\nu = 1/\mu\sigma a^2$ is a characteristic decay rate and $j_1(x)$ is
    the spherical Bessel function of order one. Show that the magnetic field at
    the center of the sphere can be written explicitly in terms of the error
    function $\mathrm{erf}(x)$ as
    \begin{equation*}
        B_z(0,t) = B_0\left[\mathrm{erf}\frac{1}{\sqrt{4vt}}
        - \frac{1}{\sqrt{\pi\nu t}}\exp\left(-\frac{1}{4\nu t}\right)\right]
    \end{equation*}
    \item Show that the total magnetic energy at the time $t>0$ can be written
    \begin{equation*}
        W_m = \frac{6B_0^2a^3}{\mu}\int_0^\infty e^{-2\nu tk^2}j_1^2(k)dk
    \end{equation*}
    Show that at long times ($\nu t\gg1$) the magnetic energy decays
    asymptotically as
    \begin{equation*}
        W_m \rightarrow \frac{\sqrt{2\pi}B_0^2a^3}{24\mu(\nu t)^{3/2}}
    \end{equation*}
    \item Find a corresponding expression for the asymptotic form of the vector
    potential (at fixed $r, \ \theta$ and $\nu t\rightarrow\infty$) and show
    that it decays as $(\nu t)^{3/2}$ as well. Since the energy is quadratic in
    the field strength, there seems to be a puzzle here. Show by numerical or
    analytic means that the behavior of the magnetic field at time $t$ is such
    that, for distances small compared to $R = a(\nu t)^{1/2} \gg a$, the field
    is uniform with strength $(B_0/6\pi^{1/2})(\nu t)^{3/2}$, and for distances
    large compared to $R$, the field is essentially the original dipole field.
    Explain physically.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
The time-varying magnetic field for $t>0$ in Problem \ref{problem:5_35} induces
an electric field and causes current to flow.
\begin{enumerate}[label=(\alph*)]
    \item What components of electric field exist? Determine integral
    expressions for the components of the electric field and find a simple
    explicit form of the current density $\mathbf{J}=\sigma\mathbf{E}$ at
    $t=0^+$. Compare your result with the current density of Problem
    \ref{problem:5_35}a. Find the asymptotic behavior of the electric fields
    in time.
    \item With Ohm's law and the electric fields found in part a, show that the
    total power dissipated in the resistive medium can be written
    \begin{equation*}
        P = \frac{12B_0^2a^3\nu}{\mu}\int_0^\infty e^{-2\nu tk^2}k^2j_1^2(k)dk
    \end{equation*}
    Note that the power is the negative time derivative of the magnetic energy
    $W_m$.
    \item Because of Ohm's law, the total electric energy is $W_e = P/2\sigma$.
    The total energy is the sum of $W_e$ and $W_m$; its time derivative should
    be the negative of the power dissipation. Show that the neglect of the
    energy in the electric field is the same order of approximation as neglect
    of the displacement current in the equations governing the magnetic field.
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\end{document}
