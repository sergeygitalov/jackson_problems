\documentclass[jackson_problems.tex]{subfiles}

\usepackage{enumerate}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{bbm}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{multicol}
\usepackage{slashed}
\usepackage{hyperref}
\usepackage{siunitx}

\numberwithin{equation}{section}

\begin{document}

\begin{problem}
    Calculate the multipole moments $q_{lm}$ of the charge distributions in
    parts a and b. Try to obtain results for the nonvanishing elements valid for
    all $l$, but in each case find the first two sets of nonvanishing moments at
    the very least.
    \begin{enumerate}[label=(\alph*)]
        \item Four charges: two of magnitude $q$ at $(a,0,0)$ and $(0,a,0)$; and
        two of magnitude $-q$ at $(-a,0,0)$ and $(0,-a,0)$.
        \item Three charges: a charge at the origin with magnitude $-2q$ and two
        charges of magnitude $q$ at $(0,0,\pm a)$.
        \item For the charge distribution in part b write down the multipole
        expansion for the potential. Keeping only the lowest-order term in the
        expansion, plot the potential in the $xy$-plane as a function of
        distance from the origin for distances greater than $a$.
        \item Calculate directly from Coulomb's law the exact potential in part
        b in the $xy$-plane. Plot it as a function of distance and compare with
        the result found in part c.
    \end{enumerate}
    Divide out the asymptotic form in parts c and d to see the behavior at large
    distances more clearly.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    A point dipole with dipole moment $\mathbf{p}$ is located at the point
    $\mathbf{x}_0$. From the properties of the derivative of a Dirac delta 
    function, show that for calculation of the potential $\Phi$ or the energy
    of a dipole in an external field, the dipole can be described by an
    effective charge density
    \begin{equation*}
        \rho_{\text{eff}}(\mathbf{x}) 
        = -\mathbf{p}\cdot\nabla\delta(\mathbf{x}-\mathbf{x}_0)
    \end{equation*}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    The $l$th term in the multipole expansion of the potential is specified by
    the $2l+1$ multipole moments $q_{lm}$. On the other hand, the Cartesian
    multipole moments,
    \begin{equation*}
        Q^{(l)}_{\alpha\beta\gamma} 
        = \int\rho(\mathbf{x})x^\alpha y^\beta z^\gamma d^3x,
    \end{equation*}
    with $\alpha,\beta,\gamma\in\mathbb{Z}^+$ subject to the constraint
    $\alpha+\beta+\gamma=l$, are $(l+1)(l+2)/2$ in number. Thus, for $l>1$ there
    are more Cartesian multipole moments than seem necessary to describe the
    term in the potential whose radial dependence is $r^{-l-1}$.

    Show that while $q_{lm}$ transform under rotations as irreducible spherical
    tensors of rank $l$, the Cartesian multipole moments correspond to reducible
    spherical tensors of ranks $l$, $l-2$, $l-4$, $\ldots$, $l_{\text{min}}$,
    where $l_{\text{min}} = 0$ or 1 for $l$ even or odd respectively. Check that
    the number of different tensorial components adds up to the total number of
    Cartesian tensors. Why are only the $q_{lm}$ needed in the multipole
    expansion.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
\hspace{2em}
\begin{enumerate}[label=(\alph*)]
    \item Prove the following theorem: for any arbitrary charge distribution
    $\rho(\mathbf{x})$ the values of the $(2l+1)$ moments of the first
    nonvanishing multipole are independent of the origin of the coordinate axes,
    but the values of all higher multipole moments do in general depend on the
    choice of origin. (The different moments $q_{lm}$ for fixed $l$ depend, of
    course, on the orientation of the axes.)
    \item A charge distribution has multipole moments 
    $q,\mathbf{p},Q_{ij},\ldots$ with respect to one set of coordinate axes, and
    moments $q',\mathbf{p}',Q_{ij}^\prime,\ldots$ with respect to another set
    whose axes are parallel to the first, but whose origin is located at the
    point $\mathbf{R}=(X,Y,Z)$ relative to the first. Determine explicitly the
    connections between the monopole, dipole, and quadrupole moments in the two
    coordinate axes.
    \item If $q\not=0$, can $\mathbf{R}$ be found so that $\mathbf{p}'=0$?
    If $q\not=0$, $\mathbf{p}\not=0$, or at least $\mathbf{p}\not=0$, can
    $\mathbf{R}$ be found so that $Q_{ij}^\prime=0$?
\end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    A localized charge density $\rho(x,y,z)$ is placed in an external
    electrostatic field described by a potential $\Phi^{(0)}(x,y,z)$. The
    external potential varies slowly in space over the region where the charge
    density is different from zero.
    \begin{enumerate}[label=(\alph*)]
        \item From first principles calculate the total force acting on the
        charge distribution as an expansion in multipole moments times 
        derivatives of the electric field, up to an including the quadrupole
        moments. Show that the force is
        \begin{equation*}
            \mathbf{F} = q\mathbf{E}^{(0)}(0)
            + \{\nabla[\mathbf{p}\cdot\mathbf{E}^{(0)}]\}_0
            + \left\{\nabla\left[\frac{1}{6}\sum_{j,k}Q_{jk}
            \frac{\partial{E_j^{(0)}}}{\partial{x_k}}\right]\right\}_0 + \cdots
        \end{equation*}
        Compare this to the expansion of the energy $W$,
        \begin{equation*}
            W = q\Phi(0) - \mathbf{p}\cdots\mathbf{E}(0)
            -\left.\frac{1}{6}\sum_{jk}Q_{jk}
            \frac{\partial{E_j^{(0)}}}{\partial{x_k}}\right|_0.
        \end{equation*}
        Note that $W$ is a number - is is not a function of $\mathbf{x}$ that
        can be differentiated! What is the connection to $\mathbf{F}$?
        \item Repeat the calculation of part a for the total torque. For
        simplicity, evaluate only one Cartesian component of the torque, say
        $N_1$. Show that the component is
        \begin{equation*}
        \begin{split}
            N_1 &= [\mathbf{p}\times\mathbf{E}^{(0)}(0)]_1 \\
            &+ \frac{1}{3}\left[
            \frac{\partial}{\partial{x_3}}\left(\sum_jQ_{2j}E_j^{(0)}\right)
            - \frac{\partial}{\partial{x_2}}\left(\sum_jQ_{3j}E_j^{(0)}\right)
            \right]_0 + \cdots
        \end{split}
        \end{equation*}
    \end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    A nucleus with quadrupole moment $Q$ finds itself in a cylindrically
    symmetric electric field with a gradient $(\partial{E_z}/\partial{z})_0$
    along the $z$ axis at the position of the nucleus.
    \begin{enumerate}[label=(\alph*)]
        \item Show that the energy of the quadrupole interaction is
        \begin{equation*}
            W = -\frac{e}{4}Q\left(\frac{\partial{E_z}}{\partial{z}}\right)_0
        \end{equation*}
        \item If it is known that $Q = \qty{2e-28}{\m^2}$ and that $W/h$ is
        $\qty{10}{\MHz}$, where $h$ is Planck's constant, calculate
        $(\partial{E_z}/\partial{z})_0$ in units of $e/4\pi a^3_0$, where
        $a_0$ is the Bohr radius in hydrogen.
        \item Nuclear charge distributions can be approximated by a constant
        charge density throughout a spheroidal volume of semimajor axis $a$ and
        semiminor axis $b$. Calculate the quadrupole moment of such a nucleus,
        assuming that the total charge is $Ze$. Given that $\mathrm{Eu}^{153}$
        ($Z=63$) has a quadrupole moment $Q=\qty{2.5e-28}{\m^2}$ and a mean
        radius
        \begin{equation*}
            R = (a+b)/2 = \qty{7e-15}{\m},
        \end{equation*}
        determine the fractional difference in radius $(a-b)/R$.
    \end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
\end{problem}
\begin{proof}
    A localized distribution of charge has a charge density
    \begin{equation*}
        \rho(\mathbf{r}) = \frac{1}{64\pi}r^2e^{-r}\sin^2\theta
    \end{equation*}
    \begin{enumerate}[label=(\alph*)]
        \item Make a multipole expansion of the potential due to this charge
        density and determine all the nonvanishing multipole moments. Write down
        the potential at large distances as a finite expansion in Legendre
        polynomials.
        \item Determine the potential explicitly at any point in space, and show
        that near the origin, correct to $r^2$ inclusive,
        \begin{equation*}
            \Phi(\mathbf{r})\approx\frac{1}{4\pi}
            \left[\frac{1}{4} - \frac{r^2}{120}P_2(\cos\theta)\right].
        \end{equation*}
    \end{enumerate}
\end{proof}

\begin{problem}
    A very long, right circular, cylindrical shell of dielectric constant
    $\epsilon$ and inner and outer radii $a$ and $b$, respectively, is placed
    in a previously uniform electric field $E_0$ with its axis perpendicular
    to the field. The medium inside and outside the cylinder has a dielectric
    constant of unity.
    \begin{enumerate}[label=(\alph*)]
        \item Determine the potential and electric field in the three regions,
        neglecting end effects.
        \item Sketch the lines of force for a typical case of $b\approx2a$.
        \item Discuss the limiting forms of your solution appropriate for a
        solid dielectric cylinder in a uniform field, and a cylindrical cavity
        in a uniform dielectric.
    \end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    A point charge $q$ is located in free space a distance $d$ from the center
    of a dielectric sphere of radius $a$ ($a<d$) and dielectric constant 
    $\epsilon$.
    \begin{enumerate}[label=(\alph*)]
        \item Find the potential at all points in space as an expansion in
        spherical harmonics.
        \item Calculate the rectangular components of the electric field near
        the center of the sphere
        \item Verify that, in the limit $\epsilon\rightarrow\infty$, your result
        is the same as that for the conducting sphere.
    \end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    Two concentric conducting spheres of inner and outer radii $a$ and $b$,
    respectively, carry charges $\pm Q$. The empty space between the spheres
    is half-filled by a hemispherical shell of dielectric (of dielectric
    constant $\epsilon$).
    \begin{enumerate}[label=(\alph*)]
        \item Find the electric field everywhere between the spheres.
        \item Calculate the surface-charge distribution on the inner sphere.
        \item Calculate the polarization-charge density induced on the surface
        of the dielectric at $r=a$.
    \end{enumerate}
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    Test the Clausius-Mossotti relation between dielectric constants and density
    for air and pentane in the ranges tabulated. Does it hold exactly?
    Approximately? If approximately, discuss the fractional variations in
    density and $(\epsilon-1)$. For pentane, compare the Clausius-Mossotti
    relation to the cruder relation $(\epsilon-1)\propto$ density.
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    Water vapor is a polar gas whose dielectric constant exhibits an appreciable
    temperature dependence. Assuming that water vapor obeys the ideal gas law, 
    calculate the molecular polarizability as a function of inverse temperature 
    and plot it. From the slope of the curve, deduce a value for the permanent 
    dipole moment of the $\mathrm{H_2O}$ molecule (express the dipole moment in 
    coulomb-meters).
\end{problem}
\begin{proof}
\end{proof}

\begin{problem}
    Two long, coaxial, cylindrical conducting surfaces of radii $a$ and $b$ are
    lowered vertically into a liquid dielectric. If the liquid rises an average
    height $h$ between the electrodes when a potential difference $V$ is
    established between them, show that the susceptibility of the liquid is
    \begin{equation}
        \chi_e = \frac{(b^2-a^2)\rho gh\ln(b/a)}{V^2},
    \end{equation}
    where $\rho$ is the density of the liquid, $g$ is the acceleration due to
    gravity, and the susceptibility of air is neglected.
\end{problem}
\begin{proof}
\end{proof}

\end{document}
